### Discontinued
This repo is discontinued, please refer to the github one over [here](https://github.com/nicofirst1/ML_notes)

### Info
Summaries for the Machine Learning course held by professor Luca Iocchi at the Sapienza university.

The summaries can be found in the main.pdf file together with solved exercises.

There can be some errors in either the theory or the exercises, so if you spot one you are more than welcomed to point it out.

If you find this repo to be of some utiliy please share and star it.

Thanks